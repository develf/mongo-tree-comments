#!/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime as dt
from pymongo import MongoClient
import json
from bson.objectid import ObjectId


client = MongoClient('')
db = client.nested
comments = db.comments

def add_comment(cid, pid=None, b='testcom'):
    """cid - ObjectID for comment
    pid - ObjectId for parent comment
    b - comment body
    """

    query = {
        '_id':cid,
        'path':pid, # path like in Model Tree Structures with Materialized Paths
        'created':dt.now(), # timestamp
        'body':b,
        'depth':0, # comment level
        'orderid':1, # comment position
        'answers':0 # counter for answers to comment
    }

    # If parent comment exists
    if pid:
        parent = comments.find_one({"_id":pid})

        # build new comment path:
        if parent['path']:
            query['path']=[ pid ] + parent['path']
        else:
            query['path']=[ pid ]

        # calculate new comment level:
        query['depth']= parent['depth']+1

        # calculate new comment position:
        query['orderid'] = parent['orderid'] + parent['answers'] + 1

        # increase answer counter by one:
        comments.update({"_id":  {"$in":query['path']}}, {"$inc":{'answers':1}}, multi=True)
        # shift all next comments by one:
        comments.update({"orderid":{"$gte":query['orderid']} }, {"$inc":{'orderid':1}}, multi=True)

    # If parent comment not exists
    else:
        # find last order id:
        last_order_id = comments.find({}).sort([('orderid',-1)]).limit(1)
        last_order_id = list(last_order_id)
        if last_order_id:
            query['orderid'] = last_order_id[0]['orderid'] + 1
        # if last order id is not exist, then it first comment:
        else:
            query['orderid'] = 1

    comments.insert(query)

def prt():
    cms = comments.find({}).sort([('orderid',1)])
    for cm in cms:
        ws = ' ' * cm['depth']
        print '%s%s - %s' % (ws, cm['created'], cm['body'])

def filldb():
    comments.remove()
    cmid = []
    for i in range(20):
        cmid.append(ObjectId())

    add_comment(cmid[0], b='a')
    add_comment(cmid[1], cmid[0], b='b')
    add_comment(cmid[2], cmid[0], b='c')
    add_comment(cmid[3], cmid[0], b='d')
    add_comment(cmid[4], cmid[3], b='e')
    add_comment(cmid[5], cmid[4], b='f')
    add_comment(cmid[6], cmid[5], b='g')
    add_comment(cmid[7], cmid[6], b='h')
    add_comment(cmid[8], cmid[4], b='i')
    add_comment(cmid[9], cmid[4], b='j')
    add_comment(cmid[10], cmid[0], b='k')
    add_comment(cmid[11], b='l')
    add_comment(cmid[12], cmid[1], b='m')
    add_comment(cmid[13], cmid[12], b='n')
    add_comment(cmid[14], cmid[13], b='o')
    add_comment(cmid[15], cmid[6], b='p')
    add_comment(cmid[16], cmid[11], b='q')
    add_comment(cmid[17], cmid[9], b='r')
    add_comment(cmid[18], b='s')
    add_comment(cmid[19], cmid[1], b='t')

filldb()
prt()
